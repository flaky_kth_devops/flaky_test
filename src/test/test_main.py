import unittest
from src.main.main import unexpected_behaviour

class test_behaviour(unittest.TestCase):

    def test_unexpected(self):
        self.assertTrue(not unexpected_behaviour())


if __name__ == '__main__':
    unittest.main()
